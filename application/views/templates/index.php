<div class="breadcrumbs">
		<div class="col-sm-4">
				<div class="page-header float-left">
						<div class="page-title">
								<h1> Ordenes de Servicio</h1>
						</div>
				</div>
		</div>
		<div class="col-sm-8">
				<div class="page-header float-right">
						<div class="page-title">
								<ol class="breadcrumb text-right">
										<li><a href="create">Crear</a></li>
								</ol>
						</div>
				</div>
		</div>
</div>

<div class="content mt-3">
  <div class="animated fadeIn">
                  <div class="row">
                  <div class="col-lg-9">
                      <div class="card">
                          <div class="card-header">
                              <strong class="card-title"></strong>
                          </div>
                          <div class="card-body">
														<?php if($this->session->flashdata('correcto'))?>
																<span class="badge badge-primary">Creado Correcto</span>

                              <table class="table">
                                <thead>
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Estado</th>
                                    <th scope="col">Asunto</th>
                                    <th scope="col">Descripcion</th>
																		<th scope="col">Marca</th>
																		<th scope="col">Modelo</th>
																		<th scope="col">Imprimir</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                              foreach ($peticiones as $peticion) {
                                echo "<tr>
                                <th scope='row'>".$peticion->id."</th>".
                                "<td>".$peticion->nombre."</td>".
                                "<td>".$peticion->estado."</td>".
                                "<td>".$peticion->asunto."</td>".
                                "<td>".$peticion->descripcion."</td>".
																"<td>".$peticion->marca."</td>".
																"<td>".$peticion->modelo."</td>".
																"<td><a href='welcome/view/".$peticion->id."'  target='_blank' class='btn btn-warning btn-sm'>Imprimir</a></td>".
                                "</tr>";
                              }
                             ?>
                           </tr>
                        </tbody>
                    </table>
                          </div>
                      </div>
                  </div>

                      </div>
                  </div>
              </div>
