<div class="breadcrumbs">
		<div class="col-sm-4">
				<div class="page-header float-left">
						<div class="page-title">
								<h1>Orden de Servicio</h1>
						</div>
				</div>
		</div>
		<div class="col-sm-8">
				<div class="page-header float-right">
						<div class="page-title">
								<ol class="breadcrumb text-right">
										<li><a href="list">Consultar</a></li>
								</ol>
						</div>
				</div>
		</div>
</div>
				<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                  <div class="col-lg-9">
                    <div class="card">
                      <div class="card-header">
                        <strong>Help Desk</strong>
                      </div>
                      <div class="card-body card-block">
                        <form action="store" method="post" enctype="multipart/form-data" class="form-horizontal">
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="estado" class=" form-control-label">Estado</label></div>
                            <div class="col-12 col-md-9">
                              <select name="estado" id="SelectLm" class="form-control-sm form-control">
                                <option value="0">Please select</option>
                                <option value="1">Abierto</option>
                                <option value="2">Asignado</option>
                              </select>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="nombre" class=" form-control-label">Nombre</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="nombre" placeholder="Teclea Nombre" class="form-control"></div>
                          </div>
													<div class="row form-group">
                            <div class="col col-md-3"><label for="nombre" class=" form-control-label">Telefono</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="telefono" placeholder="" class="form-control"></div>
                          </div>
													<div class="row form-group">
														<div class="col col-md-3"><label for="nombre" class=" form-control-label">Correo</label></div>
														<div class="col-12 col-md-9"><input type="text" id="text-input" name="correo" placeholder="" class="form-control"></div>
													</div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="prioridad" class=" form-control-label">Prioridad</label></div>
                            <div class="col-12 col-md-9">
                              <select name="prioridad" id="SelectLm" class="form-control-sm form-control">
                                <option value="0">Please select</option>
                                <option value="1">Baja</option>
                                <option value="2">Media</option>
                                <option value="3">Alta</option>
                                <option value="4">Critica</option>
                              </select>
                            </div>
                          </div>
													<div class="row form-group">
                            <div class="col col-md-3"><label for="direccion" class=" form-control-label">Dirección</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="direccion" class="form-control"></div>
                          </div>
													<div class="row form-group">
                            <div class="col col-sm-3"><label for="asunto" class=" form-control-label">Marca</label></div>
                            <div class="col-12 col-sm-5"><input type="text" id="text-input" name="marca" class="form-control"></div>
                          </div>
													<div class="row form-group">
                            <div class="col col-sm-3"><label for="asunto" class=" form-control-label">Modelo</label></div>
                            <div class="col-12 col-sm-5"><input type="text" id="text-input" name="modelo" class="form-control"></div>
                          </div>
													<div class="row form-group">
                            <div class="col col-sm-3"><label for="asunto" class=" form-control-label">No. Serie</label></div>
                            <div class="col-12 col-sm-5"><input type="text" id="text-input" name="serie" class="form-control"></div>
                          </div>
													<div class="row form-group">
                            <div class="col col-sm-3"><label for="asunto" class=" form-control-label">Descripcion Breve del problema</label></div>
                            <div class="col-12 col-sm-5"><input type="text" id="text-input" name="asunto" class="form-control"></div>
                          </div>
													<div class="row form-group">
                            <div class="col col-md-3"><label for="descripcion" class=" form-control-label">Problema</label></div>
                            <div class="col-12 col-md-9"><textarea name="descripcion" id="textarea-input" rows="9" placeholder="" class="form-control"></textarea></div>
                          </div>
													<div class="row form-group">
														<div class="col col-md-3"><label for="file" class=" form-control-label">Adjunto</label></div>
														<div class="col-12 col-md-9"><input id="file" name="file" class="form-control-file" type="file"></div>
													</div>

													<div class="row form-group">
	                          <div class="col col-md-3"><label class=" form-control-label">Caracteristicas del Equipo</label></div>
	                          <div class="col col-md-9">
	                            <div class="form-check">
	                              <div class="checkbox">
	                                <label for="checkbox1" class="form-check-label ">
	                                  <input id="checkbox1" name="checkbox1" value="option1" class="form-check-input" type="checkbox">Procesador
	                                </label>
	                              </div>
	                              <div class="checkbox">
	                                <label for="checkbox2" class="form-check-label ">
	                                  <input id="checkbox2" name="checkbox2" value="option2" class="form-check-input" type="checkbox">Memoria
	                                </label>
	                              </div>
	                              <div class="checkbox">
	                                <label for="checkbox3" class="form-check-label ">
	                                  <input id="checkbox3" name="checkbox3" value="option3" class="form-check-input" type="checkbox">Disco Duro
	                                </label>
	                              </div>
	                            </div>
	                          </div>
													</div>

												</div> <!-- card-body-->

													<div class="card-footer">
														<button type="submit" class="btn btn-primary btn-sm">
															<i class="fa fa-dot-circle-o"></i> Submit
														</button>
														<button type="reset" class="btn btn-danger btn-sm">
															<i class="fa fa-ban"></i> Reset
														</button>
													</div>
												</div>
											</div>
